"""This module contains functions for managing transactions in a megamart."""

from datetime import datetime
from typing import Dict, Tuple, Optional
from DiscountType import DiscountType
from PaymentMethod import PaymentMethod
from FulfilmentType import FulfilmentType
from Transaction import Transaction
from Item import Item
from Customer import Customer
from Discount import Discount

from RestrictedItemException import RestrictedItemException
from PurchaseLimitExceededException import PurchaseLimitExceededException
from InsufficientStockException import InsufficientStockException
from FulfilmentException import FulfilmentException


# You are to complete the implementation for the eight methods below:
# START


def is_not_allowed_to_purchase_item(
    item: Item, customer: Optional[Customer], purchase_date_string:
        Optional[str]) -> bool:
    """
    Determine if a customer is allowed to purchase a specific item.

    This function returns True if the customer is not allowed to purchase
    the specified item, False otherwise. If an item object or the purchase
    date string was not actually provided, an Exception is raised. Items
    that are under the alcohol, tobacco, or knives category may only be sold
    to customers who are aged 18+ and have their ID verified. An item
    potentially belongs to many categories - as long as it belongs to at
    least one of the three categories above, restrictions apply to that
    item. The checking of an item's categories against restricted categories
    should be done in a case-insensitive manner. For example, if an item A
    is in the category ['Alcohol'] and item B is in the category [
    'ALCOHOL'], both items A and B are identified as restricted items. Even
    if the customer is aged 18+ and is verified, they must provide/link
    their member account to the transaction when purchasing restricted
    items. Otherwise, if a member account is not provided, they are not
    allowed to purchase the restricted item even if normally allowed to.
    Customers can optionally provide their date of birth in their profile.
    The purchase date string should be of the format dd/mm/yyyy. The age of
    the customer is calculated from their specified date of birth, which is
    also of the format dd/mm/yyyy. If an item is a restricted item but the
    purchase or birthdate is in the incorrect format, an Exception is
    raised. A customer whose date of birth is 01/08/2005 is considered to be
    age 18+ only on or after 01/08/2023.
    """
    if item is None:
        raise ValueError("Item is missing")

    if purchase_date_string is None:
        return True

    restricted = ["alcohol", "tobacco", "knives"]
    categories = [x.lower() for x in item.categories]

    if not set(restricted) & set(categories):
        return False

    if customer is None or customer.date_of_birth is None:
        return True

    try:
        purchase_date_obj = datetime.strptime(purchase_date_string, "%d/%m/%Y")
        birth_date_obj = datetime.strptime(customer.date_of_birth, "%d/%m/%Y")
    except ValueError as exception:
        raise ValueError("Incorrect data format, should be DD/MM/YYYY") from \
            exception

    if (
        customer.membership_number is None
        or not customer.id_verified
        or purchase_date_obj.year - birth_date_obj.year < 18
    ):
        return True

    if purchase_date_obj.year - birth_date_obj.year == 18:
        if purchase_date_obj.month < birth_date_obj.month or (
            purchase_date_obj.month == birth_date_obj.month
            and purchase_date_obj.day < birth_date_obj.day
        ):
            return True

    return False


def get_item_purchase_quantity_limit(
    item: Item, items_dict: Dict[str, Tuple[Item, int, Optional[int]]]
) -> Optional[int]:
    """
    For a given item, returns the integer purchase quantity limit.

    If an item object or items dictionary was not actually provided,
    an Exception should be raised. If the item was not found in the items
    dictionary or if the item does not have a purchase quantity limit,
    None should be returned. The items dictionary (which is a mapping from
    keys to values) contains string item IDs as its keys, and tuples
    containing an item object, integer stock level and an optional integer
    purchase quantity limit (which may be None) that correspond to their
    respective item ID as values.
    """
    # Check that an item object and items dictionary are actually provided.
    if item is None or items_dict is None:
        raise ValueError("Item or items_dict cannot be None")

    if item.id not in items_dict:
        return None

    if items_dict[item.id][2] is None:
        return None

    return items_dict[item.id][2]


def is_item_sufficiently_stocked(
    item: Item,
    purchase_quantity: int,
    items_dict: Dict[str, Tuple[Item, int, Optional[int]]],
) -> bool:
    """
    Determine if the item is sufficiently stocked.

    For a given item, returns True if the purchase quantity does not exceed
    the currently available stock, or False if it exceeds, or the item was
    not found in the item's dictionary. If an item object, purchase quantity
    or items dictionary was not actually provided, an Exception should be
    raised. Purchase quantity should be a minimum of 1, and stock level is
    always a minimum of 0. Otherwise, an Exception should be raised for each
    of these situations. The items dictionary (which is a mapping from keys
    to values) contains string item IDs as its keys, and tuples containing
    an item object, integer stock level and an optional integer purchase
    quantity limit ( which may be None) that correspond to their respective
    item ID as values.
    """
    # Check that an item object, purchase quantity, and items dictionary are
    # actually provided.
    if item is None or purchase_quantity is None or items_dict is None:
        raise ValueError("Item or purchase_quantity or items_dict cannot be "
                         "None")
    if item.id not in items_dict:
        return False
    if purchase_quantity < 1:
        raise ValueError("Purchase quantity must be at least 1.")
    if items_dict[item.id][1] < 0:
        raise ValueError("Stock quantity cannot be negative.")

    if purchase_quantity > items_dict[item.id][1]:
        return False
    return True


def calculate_final_item_price(
    item: Item, discounts_dict: Dict[str, Discount]
) -> float:
    """
    Calculate the final price of an item, considering any applicable discounts.

    The item's final price may change if there is currently a discount
    available for it. If an item object or discounts dictionary was not
    actually provided, an Exception should be raised. There are two types of
    discounts - it may be a percentage off the original price, or a flat
    amount off the original price. Percentage-based discounts have a value
    defined between 1 and 100 inclusive. Otherwise, an Exception should be
    thrown. For example, a percentage-type discount of value 25 means a 25%
    discount should be applied to that item. Flat-based discounts should not
    cause the item's final price to be more than its original price or be
    negative. Otherwise, an Exception should be thrown. For example,
    a flat-type discount of value 1.25 means a discount of $1.25 should be
    applied to that item. The discounts dictionary (which is a mapping from
    keys to values) contains string item IDs as its keys, and discount
    objects that correspond to their respective item ID as values. If an
    item has an associated discount, the discounts dictionary (which is a
    mapping from keys to values) will contain a key corresponding to the ID
    of that item. Otherwise, if the item does not have an associated
    discount, its final price would be the same as its original price.
    """
    if item is None or discounts_dict is None:
        raise ValueError("Item or discounts_dict cannot be None")
    for key in discounts_dict:
        if discounts_dict[key].item_id != key:
            raise ValueError("Item ID does not match the dictionary key.")
    if item.id in discounts_dict:

        if discounts_dict[item.id].type == DiscountType.FLAT:
            if 0 <= discounts_dict[item.id].value <= item.original_price:
                return round(item.original_price -
                             discounts_dict[item.id].value, 2)
            # Remove else after return
            raise ValueError("Invalid discount value: must be between 0 and "
                             "the original item price.")

        if discounts_dict[item.id].type == DiscountType.PERCENTAGE:
            if 1 <= discounts_dict[item.id].value <= 100:
                return round(
                    item.original_price * ((100 -
                                            discounts_dict[item.id].value) /
                                           100),
                    2,
                )
            # Remove else after return
            raise ValueError("Invalid discount percentage: must be between 1 "
                             "and 100.")

    return round(item.original_price, 2)


def calculate_item_savings(
    item_original_price: float, item_final_price: float
) -> float:
    """
    Calculate the savings made on an item based on original and final prices.

    Save on an item is defined as how much money you would not need to
    spend on an item compared to if you bought it at its original price. If
    an item's original price or final price was not actually provided,
    an Exception should be raised. If the final price of the item is greater
    than its original price, an Exception should be raised.
    """
    if item_final_price is None or item_original_price is None:
        raise ValueError("Both item final price and original price must be "
                         "provided.")
    if item_final_price > item_original_price:
        raise ValueError("Item final price cannot be greater than item "
                         "original price.")
    return round(item_original_price - item_final_price, 2)


def calculate_fulfilment_surcharge(
    fulfilment_type: Optional[FulfilmentType], customer: Optional[Customer]
) -> float:
    """
    Fulfilment surcharge is only applicable for deliveries.

    There is no surcharge applied in any other case. The fulfilment
    surcharge is calculated as $5 or $0.50 for every kilometre, whichever is
    greater. Surcharge value returned should have at most two decimal
    places. If a fulfilment type was not actually provided, an Exception
    should be raised. Delivery fulfilment type can only be used if the
    customer has linked their member account to the transaction, and if
    delivery distance is specified in their member profile. Otherwise,
    a FulfilmentException should be raised.
    """
    if fulfilment_type is None:
        raise ValueError("Fulfilment type must be provided.")
    if fulfilment_type == FulfilmentType.DELIVERY:
        if customer is None:
            raise FulfilmentException("Customer details must be provided for"
                                      "delivery.")
        if (customer.membership_number is None or
                customer.delivery_distance_km is None):
            raise FulfilmentException
        if round(float(customer.delivery_distance_km) * 0.50, 2) > 5.00:
            return round(float(customer.delivery_distance_km) * 0.50, 2)
        return 5.00
    return 0


def round_off_subtotal(
    subtotal: float, payment_method: Optional[PaymentMethod]
) -> (float):
    """
    Round the subtotal appropriately when paying by cash.

    The subtotal rounding is only applicable when paying by cash. There is
    no rounding performed in any other case. If the subtotal value or
    payment method was not actually provided, an Exception should be
    raised. The subtotal is rounded off to the nearest multiple of 5 cents.
    Surcharge value returned should have at most two decimal places. Cent
    amounts which have their ones-place digit as 1 - 2 or 6 - 7 will be
    rounded down. If it is 3 - 4 or 8 - 9, it will be rounded up instead. As
    the (monetary) subtotal value is provided as a float, ensure that it is
    first rounded off to two decimal places before doing the rounding.
    """
    if subtotal is None or payment_method is None:
        raise ValueError("Both subtotal and payment method must be provided.")
    subtotal = round(subtotal, 2)
    if payment_method == PaymentMethod.CASH:
        return round(0.05 * round(subtotal / 0.05), 2)
    return subtotal


def checkout(
    transaction: Transaction,
    items_dict: Dict[str, Tuple[Item, int, Optional[int]]],
    discounts_dict: Dict[str, Discount],
) -> Transaction:
    """
    Process the checkout for a transaction using the provided methods.

    It will need to utilise all the seven methods above. As part of the
    checkout process, each of the transaction lines in the transaction
    should be processed. If a transaction object, items dictionary or
    discounts dictionary was not actually provided, an Exception should be
    raised. All items in the transaction should be checked against any
    restrictions, available stock levels and purchase quantity limits. If a
    restricted item in the transaction may not be purchased by the customer
    initiating the transaction, a RestrictedItemException should be raised.
    If an item in the transaction exceeds purchase quantity limits, a
    PurchaseLimitExceededException should be raised. If an item in the
    transaction is of insufficient stock, an InsufficientStockException
    should be raised. All the transaction lines will need to be processed in
    order to calculate its respective final price after applicable discounts
    have been applied. The subtotal, surcharge and rounding amounts, as well
    as final total, total savings from discounts and total number of items
    purchased also need to be calculated for the transaction. Once the
    calculations are completed, the updated transaction object should be
    returned.
    """
    if transaction is None or items_dict is None or discounts_dict is None:
        raise ValueError("Invalid arguments provided: transaction, items_dict"
                         "and discounts_dict must all be provided.")
    total_items_purchased = 0
    subtotal = 0.0
    total_savings = 0.0
    for line in transaction.transaction_lines:
        if is_not_allowed_to_purchase_item(
            line.item, transaction.customer, transaction.date
        ):
            raise RestrictedItemException
        quantity = 0
        for line2 in transaction.transaction_lines:
            if line2.item.id == line.item.id:
                quantity += line2.quantity
        quantity_limit = get_item_purchase_quantity_limit(line.item,
                                                          items_dict)
        if quantity_limit is not None and quantity > quantity_limit:
            raise PurchaseLimitExceededException
        if not is_item_sufficiently_stocked(line.item, quantity, items_dict):
            raise InsufficientStockException
        total_items_purchased += line.quantity
        item_cost = calculate_final_item_price(line.item, discounts_dict)
        line.final_cost = float(item_cost * line.quantity)
        total_savings += float(
            calculate_item_savings(line.item.original_price, item_cost) *
            line.quantity
        )
        subtotal += line.final_cost
    surcharge = calculate_fulfilment_surcharge(
        transaction.fulfilment_type, transaction.customer
    )
    unbound_total = subtotal + surcharge
    final_total = round_off_subtotal(unbound_total, transaction.payment_method)
    rounding_amounts = float(round(final_total - unbound_total, 2))
    transaction.all_items_subtotal = subtotal
    transaction.fulfilment_surcharge_amount = surcharge
    transaction.amount_saved = total_savings
    transaction.total_items_purchased = total_items_purchased
    transaction.final_total = final_total
    transaction.rounding_amount_applied = rounding_amounts
    return transaction


# END
