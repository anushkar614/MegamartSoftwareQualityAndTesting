import unittest
import megamart
from typing import Dict, List, Tuple, Optional
from TransactionLine import TransactionLine

class TestMegaMart(unittest.TestCase):
  def test_is_not_allowed_to_purchase_item(self):
    
    # TC_F1_01

    item = megamart.Item("4", "Beer", 5.00, ["Alcohol", "Drinks"])
    customer = megamart.Customer("123","Name","01/01/2000",False,"0")
    purchase_date_string = "15/08/2023"

    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string))

    # TC_F1_02

    customer = megamart.Customer("123","Name","01/01/2000",True,"0")
    with self.assertRaises(Exception):
      megamart.is_not_allowed_to_purchase_item(None,customer,purchase_date_string)

    # TC_F1_03

    purchase_date_string = "15/8/223"

    with self.assertRaises(Exception):
      megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string)

    # TC_F1_04

    purchase_date_string = "15/08/2023"
    customer = megamart.Customer("123","Name","2000/01/01",False,"0")

    with self.assertRaises(Exception):
      megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string)

    # TC_F1_05

    customer = megamart.Customer(None,"Name","1/01/2000",False,"0")

    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string))

    # TC_F1_06

    customer = megamart.Customer("123","Name","1/01/2000",True,"0")

    self.assertFalse(megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string))
    
    # TC_F1_07

    customer = megamart.Customer("123","Name","1/01/2000",False,"0")

    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string))
    item = megamart.Item("4", "Beer", 5.00, ["ALCOHOL", "Drinks"])
    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string))

    # TC_F1_08

    item = megamart.Item("4", "Beer", 5.00, ["Alcohol", "Drinks"])
    customer = megamart.Customer("123","Name","16/08/2005",True,"0")
    purchase_date_string = "16/08/2023"

    self.assertFalse(megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string))

    # TC_F1_09

    item = megamart.Item("4", "Beer", 5.00, ["Alcohol", "Drinks"])
    customer = megamart.Customer("123","Name","17/08/2005",True,"0")
    purchase_date_string = "16/08/2023"

    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string))

    # TC_F1_10

    
    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item,customer,None))
    
    # TC_F1_11

    item = megamart.Item("4", "Beer", 5.00, ["Alcohol", "Drinks"])
    customer = megamart.Customer("123","Name","16/09/2005",True,"0")
    purchase_date_string = "16/08/2023"

    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string))

    # TC_F1_12
    item = megamart.Item("4", "Beer", 5.00, ["Alcohol", "Drinks"])
    customer = megamart.Customer("123","Name","16/08/2006",True,"0")
    purchase_date_string = "16/08/2023"

    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string))

    # TC_F1_13

    item = megamart.Item("3", "Water", 5.00, ["Drinks"])
    customer = megamart.Customer("123","Name","16/08/2002",True,"0")
    purchase_date_string = "16/08/2023"

    self.assertFalse(megamart.is_not_allowed_to_purchase_item(item,customer,purchase_date_string))
  def test_get_item_purchase_quantity_limit(self):
    item = megamart.Item("2","Coffee Powder",16.00,["Coffee","Drinks"])
    items_dict = {
      "1": (megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 20, None),
      "2": (megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 12, 2)}
    
    # TC_F2_01
    
    self.assertEqual(megamart.get_item_purchase_quantity_limit(item,items_dict),2)

    # TC_F2_02

    with self.assertRaises(Exception):
      megamart.get_item_purchase_quantity_limit(None,items_dict)

    # TC_F2_03

    with self.assertRaises(Exception):
      megamart.get_item_purchase_quantity_limit(item, None)

    # TC_F2_04

    items_dict = {
      "1": (megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 20, None),
      }
    
    self.assertEqual(megamart.get_item_purchase_quantity_limit(item,items_dict),None)

    # TC_F2_05

    items_dict = {
      "1": (megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 20, None),
      "2": (megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 12, None)}
    
    self.assertEqual(megamart.get_item_purchase_quantity_limit(item,items_dict),None)

  def test_is_item_sufficiently_stocked(self):

    item2 = megamart.Item("2","Bread",4.50,["Food","Bakery"])
    quantity = 10
    items_dict = {"1":(megamart.Item("1","Tim Tam - Chocolate",4.50,["Confectionery","Biscuits"]),20,None)}

    # TC_F3_01

    self.assertEqual(megamart.is_item_sufficiently_stocked(item2,quantity,items_dict),False)

    # TC_F3_02

    item1 = megamart.Item("1","Tim Tam - Chocolate",4.50,["Confectionery","Biscuits"])
    quantity = 25

    self.assertEqual(megamart.is_item_sufficiently_stocked(item1,quantity,items_dict),False)

    # TC_F3_03

    with self.assertRaises(Exception):
      megamart.is_item_sufficiently_stocked(None,quantity,items_dict)

    # TC_F3_04

    with self.assertRaises(Exception):
      megamart.is_item_sufficiently_stocked(item1,None,items_dict)

    # TC_F3_05

    with self.assertRaises(Exception):
      megamart.is_item_sufficiently_stocked(item1,quantity,None)

    # TC_F3_06

    quantity = 0

    with self.assertRaises(Exception):
      megamart.is_item_sufficiently_stocked(item1,quantity,items_dict)

    # TC_F3_07
    quantity = 25
    items_dict = {"1":(megamart.Item("1","Tim Tam - Chocolate",4.50,["Confectionery","Biscuits"]),-2,None)}

    with self.assertRaises(Exception):
      megamart.is_item_sufficiently_stocked(item1,quantity,items_dict)

  def test_calculate_final_item_price(self):
    item = megamart.Item("1","Tim Tam - Chocolate",4.50,["Confectionary","Food"])
    discounts_dict = {"2":megamart.Discount(megamart.DiscountType.PERCENTAGE,25,"2")}

    # TC_F4_01

    self.assertEqual(megamart.calculate_final_item_price(item,discounts_dict),4.50)

    # TC_F4_02

    with self.assertRaises(Exception):
      megamart.calculate_final_item_price(None,discounts_dict)

    # TC_F4_03

    with self.assertRaises(Exception):
      megamart.calculate_final_item_price(item,None)

    # TC_F4_04

    discounts_dict = {"1":megamart.Discount(megamart.DiscountType.PERCENTAGE,25,"1")}

    self.assertEqual(megamart.calculate_final_item_price(item,discounts_dict),3.38)

    # TC_F4_05

    discounts_dict = {"1":megamart.Discount(megamart.DiscountType.FLAT,1,"1")}

    self.assertEqual(megamart.calculate_final_item_price(item,discounts_dict),3.50)

    # TC_F4_06

    discounts_dict = {"1":megamart.Discount(megamart.DiscountType.PERCENTAGE,101,"1")}

    with self.assertRaises(Exception):
      megamart.calculate_final_item_price(item,discounts_dict)

    # TC_F4_07

    discounts_dict = {"1":megamart.Discount(megamart.DiscountType.FLAT,5,"1")}

    with self.assertRaises(Exception):
      megamart.calculate_final_item_price(item,discounts_dict)

    # TC_F4_08

    discounts_dict = {"1":megamart.Discount(megamart.DiscountType.FLAT,-1,"1")}

    with self.assertRaises(Exception):
      megamart.calculate_final_item_price(item,discounts_dict)

    # TC_F4_09

    item2 = megamart.Item("2","Coffee Powder",5,["Breakfast","Coffee"])
    discounts_dict = {"1": megamart.Discount(megamart.DiscountType.PERCENTAGE,25,"1"),"Coffee Powder":megamart.Discount(megamart.DiscountType.PERCENTAGE,20,"2")}

    with self.assertRaises(Exception):
      megamart.calculate_final_item_price(item2,discounts_dict)

    # TC_F4_10

    discounts_dict = {"1":megamart.Discount(megamart.DiscountType.PERCENTAGE,25,"2")}

    with self.assertRaises(Exception):
      megamart.calculate_final_item_price(item,discounts_dict)

    # TC_F4_11

    item = megamart.Item("1","Tim Tam - Chocolate",4.50,["Confectionary","Food"])
    discounts_dict = {"2":megamart.Discount(megamart.DiscountType.PERCENTAGE,25,"2")}

    self.assertEqual(megamart.calculate_final_item_price(item,discounts_dict),4.50)
  def test_calculate_item_savings(self):

    item_orig = 123.45
    item_final = 12.34

    # TC_F5_01

    self.assertEqual(megamart.calculate_item_savings(item_orig,item_final),111.11)

    # TC_F5_02

    item_orig = 12.34

    self.assertEqual(megamart.calculate_item_savings(item_orig,item_final),0)
    
    # TC_F5_03

    with self.assertRaises(Exception):
      megamart.calculate_item_savings(None,item_final)

    # TC_F5_04

    with self.assertRaises(Exception):
      megamart.calculate_item_savings(item_orig,None)

    # TC_F5_05

    item_orig = 12.34
    item_final = 123.45

    with self.assertRaises(Exception):
      megamart.calculate_item_savings(item_orig,item_final)
    
  def test_calculate_fulfilment_surcharge(self):
    fulfil = megamart.FulfilmentType.PICKUP
    customer = megamart.Customer("123","Name","01/01/2000",False,"12")
    # TC_F6_01

    self.assertEqual(megamart.calculate_fulfilment_surcharge(fulfil,customer),0.00)

    # TC_F6_02

    fulfil = megamart.FulfilmentType.DELIVERY

    self.assertEqual(megamart.calculate_fulfilment_surcharge(fulfil,customer),6.00)

    # TC_F6_03

    customer = megamart.Customer("123","Name","01/01/2000",False,"1")

    self.assertEqual(megamart.calculate_fulfilment_surcharge(fulfil,customer),5.00)

    # TC_F6_04

    customer = megamart.Customer("123","Name","01/01/2000",False,"13.15")

    self.assertEqual(megamart.calculate_fulfilment_surcharge(fulfil,customer),6.58)

    # TC_F6_05

    with self.assertRaises(Exception):
      megamart.calculate_fulfilment_surcharge(None,customer)

    # TC_F6_06

    with self.assertRaises(Exception):
      megamart.calculate_fulfilment_surcharge(fulfil,None)

    # TC_F6_07

    customer = megamart.Customer("123","Name","01/01/2000",False,None)

    with self.assertRaises(megamart.FulfilmentException):
      megamart.calculate_fulfilment_surcharge(fulfil,customer)
  def test_round_off_subtotal(self):

    subtotal = 15.37
    payment = megamart.PaymentMethod.CREDIT

    # TC_F7_01

    self.assertTrue(megamart.round_off_subtotal(subtotal,payment),15.37)

    payment = megamart.PaymentMethod.DEBIT

    self.assertTrue(megamart.round_off_subtotal(subtotal,payment),15.37)

    payment = megamart.PaymentMethod.CASH

    self.assertTrue(megamart.round_off_subtotal(subtotal,payment),15.35)

    # TC_F7_02

    with self.assertRaises(Exception):
      megamart.round_off_subtotal(subtotal,None)

    # TC_F7_03

    with self.assertRaises(Exception):
      megamart.round_off_subtotal(None,payment)

    # TC_F7_04

    subtotal = 15.31

    self.assertEqual(megamart.round_off_subtotal(subtotal,payment),15.30)

    subtotal = 15.37

    self.assertEqual(megamart.round_off_subtotal(subtotal,payment),15.35)

    subtotal = 15.40

    self.assertEqual(megamart.round_off_subtotal(subtotal,payment),15.40)

    # TC_F7_05

    subtotal = 15.378

    self.assertEqual(megamart.round_off_subtotal(subtotal,payment),15.40)

    subtotal = 15.328

    self.assertEqual(megamart.round_off_subtotal(subtotal,payment),15.35)

    subtotal = 15.309

    self.assertEqual(megamart.round_off_subtotal(subtotal,payment),15.30)
  def test_checkout(self):

    transaction = None
    items_dict = {"1":(megamart.Item("1","Tim Tam - Chocolate",4.50,["Confectionery","Biscuits"]),20,None)}
    discounts_dict = {"1",megamart.Discount(megamart.DiscountType.PERCENTAGE,5,"1")}

    # TC_F8_01

    with self.assertRaises(Exception):
      megamart.checkout(transaction,items_dict,discounts_dict)

    # TC_F8_02

    transaction = megamart.Transaction("22/08/2023","12:45:00")

    with self.assertRaises(Exception):
      megamart.checkout(transaction,None,discounts_dict)

    # TC_F8_03

    with self.assertRaises(Exception):
      megamart.checkout(transaction,items_dict,None)

    # TC_F8_04

    customer = None
    item_r = megamart.Item("4","Beer",5.00,["Alcohol","Drinks"])
    transaction_line = [TransactionLine(item_r,2)]
    items_dict = {"1":(item_r,20,None)}
    discounts_dict = {"4":megamart.Discount(megamart.DiscountType.PERCENTAGE,5,"4")}
    transaction.customer = None
    transaction.transaction_lines = transaction_line
    

    with self.assertRaises(megamart.RestrictedItemException):
      megamart.checkout(transaction,items_dict,discounts_dict)

    # TC_F8_05

    transaction_line = [TransactionLine(megamart.Item("1", "Tim Tam - Chocolate", 4.50, ["Confectionery", "Biscuits"]), 2)]
    items_dict = {"1":(megamart.Item("1","Tim Tam - Chocolate",4.50,["Confectionery","Biscuits"]),20,1)}
    discounts_dict = {"1",megamart.Discount(megamart.DiscountType.PERCENTAGE,5,"1")}
    transaction.transaction_lines = transaction_line

    with self.assertRaises(megamart.PurchaseLimitExceededException):
      megamart.checkout(transaction,items_dict,discounts_dict)

    # TC_F8_06

    items_dict = {"1":(megamart.Item("1","Tim Tam - Chocolate",4.50,["Confectionery","Biscuits"]),1,None)}

    with self.assertRaises(megamart.InsufficientStockException):
      megamart.checkout(transaction,items_dict,discounts_dict)

    # TC_F8_07
    fulfilment_type = megamart.FulfilmentType.PICKUP
    transaction.fulfilment_type = fulfilment_type
    transaction_line = [TransactionLine(megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 2), TransactionLine(megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 3)]
    items_dict = {
    '1' : (megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 20, None),
    '2' : (megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 20, None)}
    discounts_dict = {
    "1" : megamart.Discount(megamart.DiscountType.PERCENTAGE, 5, "1")}
    transaction.transaction_lines = transaction_line
    transaction.payment_method = megamart.PaymentMethod.CASH
    self.assertEqual(megamart.checkout(transaction,items_dict,discounts_dict).all_items_subtotal,56.54)
    
    # TC_F8_08

    customer = megamart.Customer("123","Name","01/01/2000",False, 13.15)
    fulfilment_type = megamart.FulfilmentType.DELIVERY
    items_dict = {
    '1' : (megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 20, None),
    '2' : (megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 20, None)}
    discounts_dict = {
    "1" : megamart.Discount(megamart.DiscountType.PERCENTAGE, 5, "1")}
    transaction.customer = customer
    transaction.fulfilment_type = fulfilment_type

    self.assertEqual(megamart.checkout(transaction,items_dict,discounts_dict).fulfilment_surcharge_amount,6.58)

    # TC_F8_09

    fulfilment_type = megamart.FulfilmentType.PICKUP
    payment_method = megamart.PaymentMethod.CASH
    transaction_line = [TransactionLine(megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 2), TransactionLine(megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 3)]
    items_dict = {
    '1' : (megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 20, None),
    '2' : (megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 20, None)}
    discounts_dict = {
    "1" : megamart.Discount(megamart.DiscountType.PERCENTAGE, 5, "1"), "2" : megamart.Discount(megamart.DiscountType.PERCENTAGE, 10, "2")}
    transaction.fulfilment_type = fulfilment_type
    transaction.payment_method = payment_method
    transaction.transaction_lines = transaction_line
    
    self.assertEqual(megamart.checkout(transaction,items_dict,discounts_dict).rounding_amount_applied,0.01)

    # TC_F8_10

    transaction_line = [TransactionLine(megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 2), TransactionLine(megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 3)]
    items_dict = {
    '1' : (megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 20, None),
    '2' : (megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 20, None)}
    discounts_dict = {
    "1" : megamart.Discount(megamart.DiscountType.PERCENTAGE, 5, "1"), "2" : megamart.Discount(megamart.DiscountType.PERCENTAGE, 1, "2")}
    transaction.transaction_lines = transaction_line

    self.assertEqual(megamart.checkout(transaction,items_dict,discounts_dict).rounding_amount_applied,-0.01)

    # TC_F8_11

    customer = megamart.Customer("123","Name","01/01/2000",False, 13.15)
    fulfilment_type = megamart.FulfilmentType.DELIVERY
    transaction_line = [TransactionLine(megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 2), TransactionLine(megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 3)]
    items_dict = {
    '1' : (megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 20, None),
    '2' : (megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 20, None)}
    discounts_dict = {
    "1" : megamart.Discount(megamart.DiscountType.PERCENTAGE, 25, "1"), "2" : megamart.Discount(megamart.DiscountType.PERCENTAGE, 5, "2")}
    transaction.customer = customer
    transaction.fulfilment_type = fulfilment_type
    transaction.transaction_lines = transaction_line

    self.assertEqual(megamart.checkout(transaction,items_dict,discounts_dict).final_total,58.95)

    # TC_F8_12
    
    transaction_line = [TransactionLine(megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 5), TransactionLine(megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 3)]
    items_dict = {
    '1' : (megamart.Item('1', 'Tim Tam - Chocolate', 4.50, ['Confectionery', 'Biscuits']), 20, None),
    '2' : (megamart.Item('2', 'Coffee Powder', 16.00, ['Coffee', 'Drinks']), 20, None)}
    discounts_dict = {
    "1" : megamart.Discount(megamart.DiscountType.PERCENTAGE, 5, "1")}
    transaction.transaction_lines = transaction_line
    
    self.assertEqual(megamart.checkout(transaction,items_dict,discounts_dict).total_items_purchased,8)

    
unittest.main()

