# MegaMart Checkout Software Module README

## Project Overview
MegaMart, a leading supermarket chain, aims to enhance its online shopping platform by developing a new checkout order processing and verification module. This module ensures accurate calculation of the total cost of items in a customer's cart, applies discounts, verifies product availability, and handles payment methods and delivery options. The software aims to prevent revenue loss, legal compliance issues, and customer dissatisfaction caused by incorrect pricing calculations.

## Features
The MegaMart checkout software module includes the following features:
- **Checkout Processing**: Calculate the total cost of items in the customer's cart, apply discounts, and handle payment methods.
- **Stock Verification**: Verify stock availability and purchase quantity limits for each item in the order.
- **Age Verification**: Ensure compliance with age restrictions for items such as alcohol, tobacco, or knives.
- **Fulfillment Surcharge**: Calculate surcharges for home delivery based on the distance from the store.
- **Error Handling**: Provide clear error messages to guide users through the checkout process and prevent data corruption or loss.

## Real-life Scenario
The software module addresses a scenario involving Tim Tam chocolate biscuits and coffee powder purchases, with discounts and purchase quantity limits applied.

## User Interface and Error Handling
- **Intuitive UI:** The online platform provides an intuitive and user-friendly interface accessible on various devices.
- **Clear Error Messages:** Displays clear and meaningful error messages to guide users through the checkout process.
- **Real-time Calculations:** Provides real-time calculations of total cost, savings, and surcharges for a seamless checkout experience.
- **Robust Error Handling:** Equipped with robust error handling mechanisms to gracefully manage unexpected errors and prevent data corruption or loss.
- **Itemized Receipts:** Generates accurate itemized receipts upon successful checkout and payment.

## Test Plans and Test Design
For testing purposes, a comprehensive acceptance testing including identifying quality attributes, user stories, user acceptance test scenarios (Validation i.e., a a system satisfies business requirements) as well as a test case design document has been developed to verify the functionality correctness of methods/functions (Verification i.e., to verify if we are building the product right). The unit testing cases cover various scenarios and edge cases to ensure robustness and accuracy.

## CI/CD (Continuous Integration/Continuous Deployment)
To improve the efficiency, reliability, and speed of delivering software applications to end users, by having a pipelines to check for any integration or other issues automatically, setted up the CI/CD to automatically perform static analysis tools (pycodestyles, pydocstyles, pylint, flakes8, mypy) and automated testing with the following CI/CD pipeline, checking only the main file.

## Code Review Automation via Static Analysis Tools
To automate the code review process, using static analysis tools, a report to the Board of Directors of Monash Megamart on heaps of different types of issues found.

## Method Requirements
The software module majorly includes eight methods with specific requirements, to be able to fulfil the business requirements:
1. `is_not_allowed_to_purchase_item`
2. `get_item_purchase_quantity_limit`
3. `is_item_sufficiently_stocked`
4. `calculate_final_item_price`
5. `calculate_item_savings`
6. `calculate_fulfilment_surcharge`
7. `round_off_subtotal`
8. `checkout`

## Code Implementation and Test Execution
The implementation phase involves coding the required methods in Python as well as creating and executing unit tests using the PyUnit framework. The Test-Driven Development (TDD) approach is used, ensuring closer to 100% line coverage for all implemented code. This also includes correcting the bugs analysed through the Static Analysis Tools.

For further details and assistance, refer to the provided base code and sample input data or contact the Developer.
*Name: Anushka Reddy*
*Contact: anushkar614@gmail.com*

---
**Note:** This README provides an overview of the  MegaMart checkout software module project, including its features, requirements, and testing processes. Additional documentation and code files are available for detailed implementation and testing.
