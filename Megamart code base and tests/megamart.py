from datetime import datetime
from typing import Dict, Tuple, Optional
from DiscountType import DiscountType
from PaymentMethod import PaymentMethod
from FulfilmentType import FulfilmentType
from TransactionLine import TransactionLine
from Transaction import Transaction
from Item import Item
from Customer import Customer
from Discount import Discount

from RestrictedItemException import RestrictedItemException
from PurchaseLimitExceededException import PurchaseLimitExceededException
from InsufficientStockException import InsufficientStockException
from FulfilmentException import FulfilmentException
from InsufficientFundsException import InsufficientFundsException

# You are to complete the implementation for the eight methods below:
#### START


def is_not_allowed_to_purchase_item(item: Item, customer: Customer, purchase_date_string: str) -> bool:
  """
    Returns True if the customer is not allowed to purchase the specified item, False otherwise.
  In all cases, if an item object was not actually provided, an Exception should be raised.

  Items that are under the alcohol, tobacco or knives category may only be sold to customers who are aged 18+ and have their ID verified.
  An item potentially belongs to many categories - as long as it belongs to at least one of the three categories above, restrictions apply to that item.
  The checking of an item's categories against restricted categories should be done in a case-insensitive manner.
   For example, if an item A is in the category ['Alcohol'] and item B is in the category ['ALCOHOL'], both items A and B should be identified as restricted items.
  
  The customer object represents a customer's account (personal profile), and contains some of their personal details.
  It is optional for customers to provide their date of birth in their profile. In this case, the customer will not be able to purchase restricted items as their actual age cannot be determined.

  The age of the customer is calculated as the number of full years since their specified date of birth and the purchase date. Both purchase and birth dates are of the format dd/mm/yyyy.
  Hence, a customer whose date of birth is 01/08/2005 is only considered to be age 18+ on or after 01/08/2023.
   For example, if purchasing restricted items on 31/07/2023, the customer is considered underage and may not purchase these restricted items.

  Even if the customer is actually aged 18+ and is verified (in the real world), they must provide/link their customer account to the transaction when purchasing restricted items (i.e. customer argument value is not None).
  Otherwise, if a customer account is not provided/linked to the transaction (i.e. the customer argument value is None), they will not be allowed to purchase the restricted item even if they would have been eligible.

  If an item is a restricted item but the provided purchase date or provided birth date is in the incorrect format, an Exception should be raised.
  If an item is a restricted item but the purchase date is not provided, True should be returned.
  If an item is a restricted item but the customer's birth date is not provided, True should be returned.
  If an item is a restricted item but the customer's ID is not verified, True should be returned.
  
  If an item is not a restricted item and the provided purchase date is not formatted correctly or not provided, an Exception does not need to be raised.
  If an item is not a restricted item, the customer's birth date and ID verification status does not need to be checked.
  """
  if not item:
    raise Exception("Item object must be provided.")

  if item.categories and any(category.lower() in ['alcohol', 'tobacco', 'knives'] for category in item.categories):
    if not customer:
      return False
      
    if not customer.id_verified:
      return True
    
    if not purchase_date_string:
      return True
    
    if not customer.date_of_birth:
      return True

    try:
      purchase_date_string = datetime.strptime(purchase_date_string, '%d/%m/%Y')
    except ValueError:
      raise Exception("Invalid purchase date format. It should be in dd/mm/yyyy format.")

    try:
      customer_birth_date = datetime.strptime(customer.date_of_birth, '%d/%m/%Y')
    except ValueError:
      raise Exception("Invalid customer birth date format. It should be in dd/mm/yyyy format.")

    age_18_cutoff = customer_birth_date.replace(year=customer_birth_date.year + 18)

    if purchase_date_string <= age_18_cutoff:
      return True

  return False


def get_item_purchase_quantity_limit(item: Item, items_dict: Dict[str, Tuple[Item, int, Optional[int]]]) -> Optional[int]:
  '''
  For a given item, returns the integer purchase quantity limit.
  If an item object or items dictionary was not actually provided, an Exception should be raised.
  If the item was not found in the items dictionary or if the item does not have a purchase quantity limit, None should be returned.
  The items dictionary (which is a mapping from keys to values) contains string item IDs as its keys,
   and tuples containing an item object, integer stock level and an optional integer purchase quantity limit (which may be None) that correspond to their respective item ID as values.
  '''
  if not item or not items_dict:
    raise Exception("Item object and items dictionary must be provided.")
    
  item_data = items_dict.get(item.id)
  if item_data:
    _, _, purchase_limit = item_data
    return purchase_limit

  return None


def is_item_sufficiently_stocked(item: Item, purchase_quantity: int, items_dict: Dict[str, Tuple[Item, int, Optional[int]]]) -> bool:
  """
  For a given item, returns True if the purchase quantity does not exceed the currently available stock, or False if it exceeds, or the item was not found in the items dictionary.
  If an item object, purchase quantity or items dictionary was not actually provided, an Exception should be raised.
  Purchase quantity should be a minimum of 1, and stock level is always a minimum of 0. Otherwise, an Exception should be raised for each of these situations.
  The items dictionary (which is a mapping from keys to values) contains string item IDs as its keys,
  and tuples containing an item object, integer stock level and an optional integer purchase quantity limit (which may be None) that correspond to their respective item ID as values.
  """
  if not item  or not purchase_quantity or  purchase_quantity < 1 or not items_dict:
    raise Exception("Item object, valid purchase quantity, and items dictionary must be provided.")

  item_data = items_dict.get(item.id)
  if item_data:
    _, stock_level, _ = item_data
    if stock_level < 0:
      raise Exception("Stock level must be a minimum of 0.")
    return purchase_quantity <= stock_level


def calculate_final_item_price(item: Item, discounts_dict: Dict[str, Discount]) -> float:
  """
  An item's final price may change if there is currently a discount available for it.
  If an item object or discounts dictionary was not actually provided, an Exception should be raised.
  There are two types of discounts - it may be a percentage off the original price, or a flat amount off the original price.
  Percentage-based discounts have a value defined between 1 and 100 inclusive. Otherwise, an Exception should be thrown.
   For example, a percentage-type discount of value 25 means a 25% discount should be applied to that item.
  Flat-based discounts should not cause the item's final price to be more than its original price or be negative. Otherwise, an Exception should be thrown.
   For example, a flat-type discount of value 1.25 means a discount of $1.25 should be applied to that item.
  The discounts dictionary (which is a mapping from keys to values) contains string item IDs as its keys, and discount objects that correspond to their respective item ID as values.
  If an item has an associated discount, the discounts dictionary (which is a mapping from keys to values) will contain a key corresponding to the ID of that item.
  Otherwise, if the item does not have an associated discount, its final price would be the same as its original price.
  """
  if not item or not discounts_dict:
    raise Exception("Item object and discounts dictionary must be provided.")

  discount = discounts_dict.get(item.id)
  if discount:
    if discount.type == DiscountType.PERCENTAGE:
      if not 1 <= discount.value <= 100:
        raise Exception("Percentage-based discount value must be between 1 and 100 inclusive.")
      discount_amount = item.original_price * (discount.value / 100)
    elif discount.type == DiscountType.FLAT:
      if discount.value < 0 or discount.value > item.original_price:
        raise Exception("Flat-based discount value must not exceed the original price or be negative.")
      discount_amount = discount.value
    else:
      raise Exception("Invalid discount type.")
    
    return round(item.original_price - discount_amount,2)

  return item.original_price


def calculate_item_savings(item_original_price: float, item_final_price: float) -> float:
  """
  Savings on an item is defined as how much money you would not need to spend on an item compared to if you bought it at its original price.
  If an item's original price or final price was not actually provided, an Exception should be raised.
  If the final price of the item is greater than its original price, an Exception should be raised.
  """
  if item_original_price is None or item_final_price is None:
    raise Exception("Item's original price and final price must be provided.")

  if item_final_price > item_original_price:
    raise Exception("Final price cannot be greater than original price.")
    
  return round(item_original_price - item_final_price,2)


def calculate_fulfilment_surcharge(fulfilment_type: FulfilmentType, customer: Customer) -> float:
  """
  Currently, a fulfilment surcharge is only applicable for deliveries. There is no surcharge applied in any other case.
  The fulfilment surcharge is calculated as $5 or $0.50 for every kilometre (or part thereof), whichever is greater.
  Surcharge value returned should have at most two decimal places.
  If a fulfilment type was not actually provided, an Exception should be raised.
  Delivery fulfilment type can only be used if the customer has linked their member account to the transaction, and if delivery distance is specified in their member profile.
   Otherwise, a FulfilmentException should be raised.
  """
  if not fulfilment_type:
    raise Exception("Fulfilment type must be provided.")
    
  if fulfilment_type == FulfilmentType.DELIVERY:
    if not customer:
      raise FulfilmentException
        
    if not customer.delivery_distance_km:
      raise FulfilmentException("Member account must be linked for delivery fulfilment.")
        
    surcharge_per_km = max(0.5, 5 / 1000)
    surcharge = surcharge_per_km * customer.delivery_distance_km
    return round(surcharge, 2)
    
  return 0.00


def round_off_subtotal(subtotal: float, payment_method: PaymentMethod) -> float:
  """
  Currently, subtotal rounding is only applicable when paying by cash.
  There is no rounding performed in any other case.
  If the subtotal value or payment method was not actually provided, an Exception should be raised.
  The subtotal is rounded off to the nearest multiple of 5 cents. Surcharge value returned should have at most two decimal places.
  Cent amounts which have their ones-place digit as 1 - 2 or 6 - 7 will be rounded down. If it is 3 - 4 or 8 - 9, it will be rounded up instead.
  As the (monetary) subtotal value is provided as a float, ensure that it is first rounded off to two decimal places before doing the rounding.
  """
  if payment_method != PaymentMethod.CASH:
    return subtotal

  if not subtotal or not payment_method:
    raise Exception("Subtotal value or payment method not provided")
    
  rounded_subtotal = round(subtotal, 2)
  cents = int(rounded_subtotal * 100) % 10  # Get the ones-place digit of the cents, ex - 15.53, cents = 3

  if 1 <= cents <= 2:
    number_part_except_cents = rounded_subtotal * 10 #= 155.3
    cents_in_decimal = cents/10 #0.3
    number_part_except_cents = number_part_except_cents - cents_in_decimal #155
    cents = 0
    cents = cents/100 #0.05
    number_part_except_cents = number_part_except_cents/10 #15.5
    rounded_subtotal = number_part_except_cents + cents #15.55
  elif 3 <= cents <= 7:
    number_part_except_cents = rounded_subtotal * 10 #= 155.3
    cents_in_decimal = cents/10 #0.3
    number_part_except_cents = number_part_except_cents - cents_in_decimal #155
    cents = 5
    cents = cents/100 #0.05
    number_part_except_cents = number_part_except_cents/10 #15.5
    rounded_subtotal = number_part_except_cents + cents #15.55
  elif 8 == cents:
    cents = int(rounded_subtotal * 100) % 100  # Get the ones and tens-place digit of the cents, ex - 15.58, cents = 58
    number_part_except_cents = rounded_subtotal * 100 #15.58 * 100 = 1558
    number_part_except_cents = number_part_except_cents - cents #1500
    number_part_except_cents = number_part_except_cents/100 #15
    cents = cents + 2 #60
    cents = cents/100 #0.60
    rounded_subtotal = number_part_except_cents + cents #15.55
  elif cents == 9:
    cents = int(rounded_subtotal * 100) % 100  # Get the ones and tens-place digit of the cents, ex - 15.58, cents = 58
    number_part_except_cents = rounded_subtotal * 100 #15.58 * 100 = 1558
    number_part_except_cents = number_part_except_cents - cents #1500
    number_part_except_cents = number_part_except_cents/100 #15
    cents = cents + 1 #10
    cents = cents/100 #0.60
    rounded_subtotal = number_part_except_cents + cents #15.55
  return round(rounded_subtotal,2)


def checkout(transaction: Transaction, items_dict: Dict[str, Tuple[Item, int, Optional[int]]], discounts_dict: Dict[str, Discount]) -> Transaction:
  """
  This method will need to utilise all of the seven methods above.
  As part of the checkout process, each of the transaction lines in the transaction should be processed.
  If a transaction object, items dictionary or discounts dictionary was not actually provided, an Exception should be raised.
  All items in the transaction should be checked against any restrictions, available stock levels and purchase quantity limits.
  If a restricted item in the transaction may not be purchased by the customer initiating the transaction, a RestrictedItemException should be raised.
  If an item in the transaction exceeds purchase quantity limits, a PurchaseLimitExceededException should be raised.
  If an item in the transaction is of insufficient stock, an InsufficientStockException should be raised.
  All of the transaction lines will need to be processed in order to calculate its respective final price after applicable discounts have been applied.
  The subtotal, surcharge and rounding amounts, as well as final total, total savings from discounts and total number of items purchased also need to be calculated for the transaction.
  Once the calculations are completed, the updated transaction object should be returned.
  """
  if not transaction or not items_dict or not discounts_dict:
    raise Exception("Transaction object, items dictionary, and discounts dictionary must be provided.")

  subtotal = 0.0
  total_savings = 0.0
  total_items_purchased = 0
  surcharge = 0.0

  for line in transaction.transaction_lines:
    item, purchase_quantity = line.item, line.quantity

    if is_not_allowed_to_purchase_item(item, transaction.customer, transaction.date):
      raise RestrictedItemException

    purchase_limit = get_item_purchase_quantity_limit(item, items_dict)
    if purchase_limit is not None and purchase_quantity > purchase_limit:
      raise PurchaseLimitExceededException

    if not is_item_sufficiently_stocked(item, purchase_quantity, items_dict):
      raise InsufficientStockException

    final_price = calculate_final_item_price(item, discounts_dict)
    subtotal += final_price * purchase_quantity
    total_savings += calculate_item_savings(item.original_price, final_price) * purchase_quantity
    total_items_purchased += purchase_quantity

  if transaction.fulfilment_type == FulfilmentType.DELIVERY and transaction.customer and transaction.customer.delivery_distance_km:
    surcharge = calculate_fulfilment_surcharge(transaction.fulfilment_type, transaction.customer)

  total = subtotal + surcharge
  rounded_total = round_off_subtotal(total, transaction.payment_method)
  transaction.subtotal = subtotal
  transaction.surcharge = surcharge
  transaction.rounding = rounded_total - total
  transaction.total = rounded_total
  transaction.total_savings = total_savings
  transaction.total_items_purchased = total_items_purchased

  return transaction


#### END

