import unittest
import megamart

class TestMegaMart(unittest.TestCase):
    
  def test_is_not_allowed_to_purchase_item(self):
    
    with self.assertRaises(Exception) as context:
      megamart.is_not_allowed_to_purchase_item(None, None, None)
    self.assertEqual(str(context.exception), "Item object must be provided.")
    
    item = megamart.Item('1', 'Wine', 20.00, ['Alcohol'])
    customer = megamart.Customer('123', 'John Doe', '01/01/2005', False, 5.0)
    purchase_date_string = '01/01/2022'
    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string))

    item = megamart.Item('1', 'Wine', 20.00, ['Alcohol'])
    customer = megamart.Customer('123', 'John Doe', '01/01/2005', False, 5.0)
    purchase_date_string = '2022-01-01'  # Invalid date format
    with self.assertRaises(Exception) as context:
      megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string)
      self.assertEqual(str(context.exception), "Invalid purchase date format. It should be in dd/mm/yyyy format.")
    
    item = megamart.Item('1', 'Wine', 20.00, ['Alcohol'])
    customer = megamart.Customer('123', 'John Doe', '01/31/2005', False, 5.0)
    purchase_date_string = '01-01-2022'  # Invalid date format
    with self.assertRaises(Exception) as context:
      megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string)
      self.assertEqual(str(context.exception), "Invalid customer birth date format. It should be in dd/mm/yyyy format.")
        
    # Test when customer is allowed to purchase restricted item
    item = megamart.Item('2', 'Chips', 2.00, ['Snacks'])
    customer = megamart.Customer('456', 'Jane Smith', '01/01/2005', True, 5.0)
    purchase_date_string = '01/01/2022'
    self.assertFalse(megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string))
        
    # Test when item does not belong to restricted categories
    item = megamart.Item('3', 'Shirt', 25.00, ['Clothing'])
    customer = megamart.Customer('789', 'Michael Johnson', '01/01/1990', False, 5.0)
    purchase_date_string = '01/01/2022'
    self.assertFalse(megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string))
        
    # Test when customer is not age-verified
    item = megamart.Item('4', 'Beer', 5.00, ['Alcohol'])
    customer = megamart.Customer('101', 'Sarah Lee', '01/01/1985', True, 5.0)
    purchase_date_string = '01/01/2022'
    self.assertFalse(megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string))
        
    # Test when purchase date is not provided
    item = megamart.Item('5', 'Wine', 20.00, ['Alcohol'])
    customer = megamart.Customer('202', 'David Brown', None, True, 5.0)
    purchase_date_string = None
    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string))
        
    # Test when customer's birth date is not provided
    item = megamart.Item('6', 'Wine', 20.00, ['Alcohol'])
    customer = megamart.Customer('309', 'Gautam George', None, True, 5.0)
    purchase_date_string = '01/01/2022'
    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string))
        
    # Test when customer's ID is not verified
    item = megamart.Item('7', 'Wine', 20.00, ['Alcohol'])
    customer = megamart.Customer('303', 'Emily Green', '01/01/1995', False, 5.0)
    purchase_date_string = '01/01/2022'
    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string))

    # Test when purchase date is not provided
    item = megamart.Item('8', 'Cigar', 10.00, ['AlCoHoL'])
    customer = megamart.Customer('404', 'Kevin White', '01/01/1980', True, 5.0)
    purchase_date_string = '01/01/2022'
    self.assertFalse(megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string))

    # Test when item categories contain both restricted and non-restricted categories
    item = megamart.Item('9', 'Mixed Item', 15.00, ['Alcohol', 'Snacks'])
    customer = megamart.Customer('606', 'Chris Brown', '01/01/2000', True, 5.0)
    purchase_date_string = '01/01/2022'
    self.assertFalse(megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string))

    # Test when customer's birth date is on the exact cutoff date for being 18 years old
    item = megamart.Item('10', 'Wine', 20.00, ['Alcohol'])
    customer = megamart.Customer('707', 'Alicia White', '01/08/2005', True, 5.0)
    purchase_date_string = '01/08/2023'
    self.assertTrue(megamart.is_not_allowed_to_purchase_item(item, customer, purchase_date_string))

    # Test when customer is age-verified but not linked to the transaction
    item = megamart.Item('11', 'Wine', 20.00, ['Alcohol'])
    customer = megamart.Customer('505', 'Melissa Gray', '01/01/1975', True, 5.0)
    purchase_date_string = '01/01/2022'
    self.assertFalse(megamart.is_not_allowed_to_purchase_item(item, None, purchase_date_string))

  def test_get_item_purchase_quantity_limit(self):
    item = megamart.Item('10', 'Water', 1.00, ['Beverages'])
    items_dict = {'1': (megamart.Item('1', 'Chips', 2.00, ['Snacks']), 10, 5)}
    self.assertIsNone(megamart.get_item_purchase_quantity_limit(item, items_dict))
    # Test when item has purchase quantity limit
    item1 = (megamart.Item('2', 'Chips', 2.00, ['Snacks']), 10, 5)
    items_dict = {'2': item1}
    item = megamart.Item('2', 'Chips', 2.00, ['Snacks'])
    self.assertEqual(megamart.get_item_purchase_quantity_limit(item, items_dict), 5)
        
    # Test when item has no purchase quantity limit
    item2 = (megamart.Item('3', 'Soda', 1.50, ['Beverages']), 50, None)
    items_dict = {'3': item2}
    item = megamart.Item('3', 'Soda', 1.50, ['Beverages'])
    self.assertIsNone(megamart.get_item_purchase_quantity_limit(item, items_dict))
    
    # Test when item object is not provided
    items_dict = {'4': (megamart.Item('4', 'Bread', 2.50, ['Bakery']), 30, 3)}
    with self.assertRaises(Exception):
      megamart.get_item_purchase_quantity_limit(None, items_dict)
    
    # Test when items dictionary is not provided
    item = megamart.Item('5', 'Apple', 0.75, ['Fruits'])
    with self.assertRaises(Exception):
      megamart.get_item_purchase_quantity_limit(item, None)


  def test_is_item_sufficiently_stocked(self):
    # Test when item is not in items_dict
    items_dict = {}
    item = megamart.Item('1', 'Wine', 20.00, ['Alcohol'])
    with self.assertRaises(Exception):
      megamart.is_item_sufficiently_stocked(item, 5, items_dict)
        
    # Test when purchase quantity is less than stock level
    item1 = (megamart.Item('2', 'Chips', 2.00, ['Snacks']), 10, 5)
    items_dict = {'2': item1}
    item = megamart.Item('2', 'Chips', 2.00, ['Snacks'])
    self.assertTrue(megamart.is_item_sufficiently_stocked(item, 3, items_dict))
        
    # Test when purchase quantity is equal to stock level
    item2 = (megamart.Item('3', 'Soda', 1.50, ['Beverages']), 50, None)
    items_dict = {'3': item2}
    item = megamart.Item('3', 'Soda', 1.50, ['Beverages'])
    self.assertTrue(megamart.is_item_sufficiently_stocked(item, 50, items_dict))
        
    # Test when purchase quantity is greater than stock level
    item3 = (megamart.Item('4', 'Chocolate', 1.00, ['Confectionery']), 10, None)
    items_dict = {'4': item3}
    item = megamart.Item('4', 'Chocolate', 1.00, ['Confectionery'])
    self.assertFalse(megamart.is_item_sufficiently_stocked(item, 15, items_dict))

    # Test when item object is not provided
    items_dict = {'5': (megamart.Item('5', 'Bread', 2.50, ['Bakery']), 30, 3)}
    with self.assertRaises(Exception):
      megamart.is_item_sufficiently_stocked(None, 2, items_dict)
    
    # Test when items dictionary is not provided
    item6 = megamart.Item('8', 'Apple', 0.75, ['Fruits'])
    with self.assertRaises(Exception):
      megamart.is_item_sufficiently_stocked(item6, 3, None)
    
    # Test when purchase quantity is not provided
    item4 = (megamart.Item('6', 'Milk', 1.20, ['Dairy']), 20, None)
    items_dict = {'6': item4}
    item = megamart.Item('6', 'Milk', 1.20, ['Dairy'])
    with self.assertRaises(Exception):
      megamart.is_item_sufficiently_stocked(item, None, items_dict)
    
    # Test when purchase quantity is less than 1
    item5 = (megamart.Item('7', 'Cereal', 3.50, ['Breakfast']), 5, None)
    items_dict = {'7': item5}
    item = megamart.Item('7', 'Cereal', 3.50, ['Breakfast'])
    with self.assertRaises(Exception):
      megamart.is_item_sufficiently_stocked(item, 0, items_dict)
    
    # Test when stock level is less than 0
    item7 = (megamart.Item('9', 'Juice', 2.25, ['Beverages']), -5, None)
    items_dict = {'9': item7}
    item = megamart.Item('9', 'Juice', 2.25, ['Beverages'])
    with self.assertRaises(Exception):
      megamart.is_item_sufficiently_stocked(item, 2, items_dict)

  def test_calculate_final_item_price(self):
    # Test when item has an invalid discount type
    item = megamart.Item('6', 'Snacks', 2.50, ['Snacks'])
    discount = megamart.Discount(item_id='6', type='UNKNOWN', value=10)
    discounts_dict = {'6': discount}
    with self.assertRaises(Exception) as context:
      megamart.calculate_final_item_price(item, discounts_dict)
      self.assertEqual(str(context.exception), "Invalid discount type.")
    # Test when no discount dictionary has been provided
    item = megamart.Item('1', 'Wine', 20.00, ['Alcohol'])
    discounts_dict = None
    with self.assertRaises(Exception):
      megamart.calculate_final_item_price(item, discounts_dict)
    
    # Test when item object is not provided
    discounts_dict = {}
    with self.assertRaises(Exception):
      megamart.calculate_final_item_price(None, discounts_dict)
        
    # Test when item has percentage-based discount
    item = megamart.Item('2', 'Chips', 2.00, ['Snacks'])
    discount = megamart.Discount(item_id='2', type=megamart.DiscountType.PERCENTAGE, value=20)
    discounts_dict = {'2': discount}
    self.assertEqual(megamart.calculate_final_item_price(item, discounts_dict), 1.60)
        
    # Test when item has flat-based discount
    item = megamart.Item('3', 'Soda', 1.50, ['Beverages'])
    discount = megamart.Discount(item_id='3', type=megamart.DiscountType.FLAT, value=0.50)
    discounts_dict = {'3': discount}
    self.assertEqual(megamart.calculate_final_item_price(item, discounts_dict), 1.00)
        
    # Test when item has invalid percentage-based discount value
    item = megamart.Item('4', 'Chocolate', 1.00, ['Confectionery'])
    discount = megamart.Discount(item_id='4', type=megamart.DiscountType.PERCENTAGE, value=110)
    discounts_dict = {'4': discount}
    with self.assertRaises(Exception):
      megamart.calculate_final_item_price(item, discounts_dict)
        
    # Test when item has invalid flat-based discount value
    item = megamart.Item('5', 'Soda', 1.50, ['Beverages'])
    discount = megamart.Discount(item_id='5', type=megamart.DiscountType.FLAT, value=2.00)
    discounts_dict = {'5': discount}
    with self.assertRaises(Exception):
      megamart.calculate_final_item_price(item, discounts_dict)

  def test_calculate_item_savings(self):
    # Test when item savings are valid
    item_original_price = 20.00
    item_final_price = 18.00
    self.assertEqual(megamart.calculate_item_savings(item_original_price, item_final_price), 2.00)
        
    # Test when item final price is greater than original price
    item_original_price = 20.00
    item_final_price = 25.00
    with self.assertRaises(Exception):
      megamart.calculate_item_savings(item_original_price, item_final_price)

    # Test when both original price and final price are provided
    item_original_price = 20.00
    item_final_price = 15.00
    self.assertEqual(megamart.calculate_item_savings(item_original_price, item_final_price), 5.00)
    
    # Test when original price is not provided
    item_original_price = None
    item_final_price = 15.00
    with self.assertRaises(Exception):
      megamart.calculate_item_savings(item_original_price, item_final_price)
    
    # Test when final price is not provided
    item_original_price = 20.00
    item_final_price = None
    with self.assertRaises(Exception):
      megamart.calculate_item_savings(item_original_price, item_final_price)

  def test_calculate_fulfilment_surcharge(self):
    # Test when fulfilment type is not provided
    with self.assertRaises(Exception):
      megamart.calculate_fulfilment_surcharge(None, None)

    fulfilment_type = megamart.FulfilmentType.DELIVERY
    with self.assertRaises(megamart.FulfilmentException):
      megamart.calculate_fulfilment_surcharge(fulfilment_type, None)

    # Test when fulfilment type is not DELIVERY
    customer = megamart.Customer(
    membership_number='123',  # Provide a membership number
    name='John Doe',  # Provide a name
    date_of_birth='2000-01-01',  # Provide a valid date of birth
    id_verified=True,  # Provide id_verified value
    delivery_distance_km=5
    )
    self.assertEqual(megamart.calculate_fulfilment_surcharge(megamart.FulfilmentType.PICKUP, customer), 0.00)
        
    # Test when customer is not linked for delivery fulfilment
    customer = megamart.Customer(
    membership_number='123',
    name='John Doe',
    date_of_birth='2000-01-01',
    id_verified=True,
    delivery_distance_km=None
    )
    with self.assertRaises(megamart.FulfilmentException):
      megamart.calculate_fulfilment_surcharge(megamart.FulfilmentType.DELIVERY, customer)
        
    # Test valid delivery surcharge calculation
    customer = megamart.Customer(
    membership_number='123',
    name='John Doe',
    date_of_birth='2000-01-01',
    id_verified=True,
    delivery_distance_km=10
    )
    self.assertEqual(megamart.calculate_fulfilment_surcharge(megamart.FulfilmentType.DELIVERY, customer), 5.0)

    # Test when fulfilment type is DELIVERY and distance results in a surcharge with more than 2 decimal places
    customer = megamart.Customer(
    membership_number='123',
    name='John Doe',
    date_of_birth='2000-01-01',
    id_verified=True,
    delivery_distance_km=3.3333  # This will result in a surcharge with more than 2 decimal places
    )
    surcharge = megamart.calculate_fulfilment_surcharge(megamart.FulfilmentType.DELIVERY, customer)
    self.assertAlmostEqual(surcharge, 1.67, places=2)  # Checking up to 2 decimal places

    # Test when fulfilment type is DELIVERY but customer's delivery distance is not specified
    customer = megamart.Customer(
    membership_number='123',
    name='John Doe',
    date_of_birth='2000-01-01',
    id_verified=True,
    delivery_distance_km=None  # Delivery distance is not specified
    )
    with self.assertRaises(megamart.FulfilmentException):
      megamart.calculate_fulfilment_surcharge(megamart.FulfilmentType.DELIVERY, customer)
    
    # Test when fulfilment type is DELIVERY and distance is exactly 1 km
    customer = megamart.Customer(
    membership_number='123',
    name='John Doe',
    date_of_birth='2000-01-01',
    id_verified=True,
    delivery_distance_km=1
    )
    self.assertEqual(megamart.calculate_fulfilment_surcharge(megamart.FulfilmentType.DELIVERY, customer), 0.50)

  def test_round_off_subtotal(self):
    # Test when subtotal value is None
    subtotal = None
    payment_method = megamart.PaymentMethod.CASH
    with self.assertRaises(Exception) as context:
      megamart.round_off_subtotal(subtotal, payment_method)
      self.assertEqual(str(context.exception), "Subtotal value or payment method not provided")

    # Test when payment method is None
    subtotal = 20.00
    payment_method = None
    with self.assertRaises(Exception) as context:
      megamart.round_off_subtotal(subtotal, payment_method)
      self.assertEqual(str(context.exception), "Subtotal value or payment method not provided")

    # Test when both subtotal value and payment method are None
    subtotal = None
    payment_method = None
    with self.assertRaises(Exception) as context:
      megamart.round_off_subtotal(subtotal, payment_method)
      self.assertEqual(str(context.exception), "Subtotal value or payment method not provided")

    # Test when payment method is not CASH
    self.assertEqual(megamart.round_off_subtotal(10.00, megamart.PaymentMethod.CREDIT), 10.00)
        
    # Test when payment method is CASH and subtotal rounding is not required
    self.assertEqual(megamart.round_off_subtotal(15.00, megamart.PaymentMethod.CASH), 15.00)
        
    # Test when payment method is CASH and subtotal rounding is required
    self.assertEqual(megamart.round_off_subtotal(15.50, megamart.PaymentMethod.CASH), 15.50)
    self.assertEqual(megamart.round_off_subtotal(15.01, megamart.PaymentMethod.CASH), 15.0)
    self.assertEqual(megamart.round_off_subtotal(15.52, megamart.PaymentMethod.CASH), 15.50)
    self.assertEqual(megamart.round_off_subtotal(15.03, megamart.PaymentMethod.CASH), 15.05)
    self.assertEqual(megamart.round_off_subtotal(15.04, megamart.PaymentMethod.CASH), 15.05)
    self.assertEqual(megamart.round_off_subtotal(15.05, megamart.PaymentMethod.CASH), 15.05)
    self.assertEqual(megamart.round_off_subtotal(15.56, megamart.PaymentMethod.CASH), 15.55)
    self.assertEqual(megamart.round_off_subtotal(15.07, megamart.PaymentMethod.CASH), 15.05)
    self.assertEqual(megamart.round_off_subtotal(15.58, megamart.PaymentMethod.CASH), 15.6)
    self.assertEqual(megamart.round_off_subtotal(15.09, megamart.PaymentMethod.CASH), 15.1)
    self.assertEqual(megamart.round_off_subtotal(15.10, megamart.PaymentMethod.CASH), 15.1)

    # Test when payment method is CASH and subtotal is exactly on the rounding boundary
    self.assertEqual(megamart.round_off_subtotal(15.25, megamart.PaymentMethod.CASH), 15.25)
    self.assertEqual(megamart.round_off_subtotal(15.35, megamart.PaymentMethod.CASH), 15.35)
    self.assertEqual(megamart.round_off_subtotal(15.45, megamart.PaymentMethod.CASH), 15.45)
    self.assertEqual(megamart.round_off_subtotal(15.55, megamart.PaymentMethod.CASH), 15.55)
    self.assertEqual(megamart.round_off_subtotal(15.65, megamart.PaymentMethod.CASH), 15.65)
    self.assertEqual(megamart.round_off_subtotal(15.75, megamart.PaymentMethod.CASH), 15.75)
    self.assertEqual(megamart.round_off_subtotal(15.85, megamart.PaymentMethod.CASH), 15.85)
    self.assertEqual(megamart.round_off_subtotal(15.95, megamart.PaymentMethod.CASH), 15.95)

def test_checkout(self):
  # Test when transaction, items_dict, or discounts_dict is not provided
  with self.assertRaises(Exception):
    megamart.checkout(None, None, None)

  # Test when customer is not allowed to purchase a restricted item
  restricted_item = megamart.Item('3', 'Restricted Item', 50.00, [])
  item3 = (restricted_item, 1, None)
  items_dict = {'3': item3}
  discounts_dict = {}
  transaction = megamart.Transaction('02/08/2023', '12:00:00')
  transaction.transaction_lines = [
    megamart.TransactionLine(restricted_item, 1)
  ]
  transaction.payment_method = megamart.PaymentMethod.CASH
  transaction.fulfilment_type = megamart.FulfilmentType.PICKUP
  transaction.customer = megamart.Customer('123', 'John Doe', '2000-01-01', True, 5.0)
  with self.assertRaises(megamart.RestrictedItemException):
    megamart.checkout(transaction, items_dict, discounts_dict)
  
  # Test when purchase quantity exceeds item's purchase limit
  item4 = (megamart.Item('4', 'Item with Limit', 10.00, []), 10, 5)
  items_dict = {'4': item4}
  transaction.transaction_lines = [
    megamart.TransactionLine(item4[0], 6)
  ]
  with self.assertRaises(megamart.PurchaseLimitExceededException):
    megamart.checkout(transaction, items_dict, discounts_dict)
    
  # Test when item is out of stock
  item5 = (megamart.Item('5', 'Out of Stock Item', 15.00, []), 0, None)
  items_dict = {'5': item5}
  transaction.transaction_lines = [
    megamart.TransactionLine(item5[0], 1)
  ]
  with self.assertRaises(megamart.InsufficientStockException):
    megamart.checkout(transaction, items_dict, discounts_dict)
    
  # Test successful checkout with discounts applied
  item6 = (megamart.Item('6', 'Discounted Item', 50.00, []), 2, None)
  items_dict = {'6': item6}
  discount = megamart.Discount(megamart.DiscountType.PERCENTAGE, 20.0, '6')
  discounts_dict = {'6': discount}
  transaction.transaction_lines = [
  megamart.TransactionLine(item6[0], 1)
  ]
  checkedout_transaction = megamart.checkout(transaction, items_dict, discounts_dict)
  self.assertAlmostEqual(checkedout_transaction.total_savings, 20.0)

  # Test successful checkout
  item1 = (megamart.Item('1', 'Wine', 20.00, ['Alcohol']), 10, None)
  item2 = (megamart.Item('2', 'Chips', 2.00, ['Snacks']), 20, None)
  items_dict = {'1': item1, '2': item2}
  discounts_dict = {}
  transaction = megamart.Transaction('02/08/2023', '12:00:00')
  transaction.transaction_lines = [
    megamart.TransactionLine(item1[0], 2),
    megamart.TransactionLine(item2[0], 5),
  ]
  transaction.payment_method = megamart.PaymentMethod.CASH
  transaction.fulfilment_type = megamart.FulfilmentType.PICKUP
  transaction.customer = megamart.Customer('123', 'John Doe', '2000-01-01', True, 5.0)
  checkedout_transaction = megamart.checkout(transaction, items_dict, discounts_dict)
  self.assertEqual(checkedout_transaction.total_items_purchased, 7)


if __name__ == '__main__':
  unittest.main()
